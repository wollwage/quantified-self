function parseISO8601Date(s){

      // parenthese matches:
      // year month day    hours minutes seconds  
      // dotmilliseconds 
      // tzstring plusminus hours minutes
      var re = /(\d{4})-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)(\.\d+)?(Z|([+-])(\d\d):(\d\d))/;

      var d = [];
      d = s.match(re);

      // "2010-12-07T11:00:00.000-09:00" parses to:
      //  ["2010-12-07T11:00:00.000-09:00", "2010", "12", "07", "11",
      //     "00", "00", ".000", "-09:00", "-", "09", "00"]
      // "2010-12-07T11:00:00.000Z" parses to:
      //  ["2010-12-07T11:00:00.000Z",      "2010", "12", "07", "11", 
      //     "00", "00", ".000", "Z", undefined, undefined, undefined]

      if (! d) {
      throw "Couldn't parse ISO 8601 date string '" + s + "'";
      }

      // parse strings, leading zeros into proper ints
      var a = [1,2,3,4,5,6,10,11];
      for (var i in a) {
      d[a[i]] = parseInt(d[a[i]], 10);
      }
      d[7] = parseFloat(d[7]);

      // Date.UTC(year, month[, date[, hrs[, min[, sec[, ms]]]]])
      // note that month is 0-11, not 1-12
      // see https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Date/UTC
      var ms = Date.UTC(d[1], d[2] - 1, d[3], d[4], d[5], d[6]);

      // if there are milliseconds, add them
      if (d[7] > 0) {  
      ms += Math.round(d[7] * 1000);
      }

      // if there's a timezone, calculate it
      if (d[8] != "Z" && d[10]) {
      var offset = d[10] * 60 * 60 * 1000;
      if (d[11]) {
      offset += d[11] * 60 * 1000;
      }
      if (d[9] == "-") {
      ms -= offset;
      }
      else {
      ms += offset;
      }
      }

      return new Date(ms);
      };
      (function() {
        var sleepTimes = [];
        var workTimes = [];
        var mealTimes = [];

        $.getJSON("../sleep.json", function(data) {
          $.each(data.sleep, function(item) {
            var start = parseISO8601Date(data.sleep[item].start);
            var end = parseISO8601Date(data.sleep[item].end);
            var difference = end - start;

            sleepTimes.push(difference / 3600000.0);
          });

          var barChartData = {
            labels: [],
            datasets: [
              {
                fillColor: "#00aaff",
                strokeColor: "#00aaff",
                data: sleepTimes
              }
            ]};

          $.each(sleepTimes, function(item) {
            barChartData.labels.push(parseISO8601Date(data.sleep[item].start).toDateString());
          });

          var barChar = new Chart(document.getElementById("canvas").getContext("2d")).Bar(barChartData, { animationSteps: 100, scaleOverride: true, scaleSteps: 24, scaleShowGridLines: false, scaleStepWidth: 1.0, scaleStartValue: 0.0, scaleShowLabels: true });
        });

        $.getJSON("../work.json", function(data) {
          var times = [];
          $.each(data.work, function(item) {
            var start = parseISO8601Date(data.work[item].start);
            var end = parseISO8601Date(data.work[item].end);
            var difference = end - start;

            times.push(difference / 3600000.0);
            workTimes.push(difference / 3600000.0);
          });

          var barChartData = {
            labels: [],
            datasets: [
              {
                fillColor: "#00aaff",
                strokeColor: "#00aaff",
                data: times
              }
            ]};

          $.each(times, function(item) {
            barChartData.labels.push(parseISO8601Date(data.work[item].start).toDateString());
          });

          var barChar = new Chart(document.getElementById("work").getContext("2d")).Bar(barChartData, { animationSteps: 100, scaleOverride: true, scaleSteps: 24, scaleShowGridLines: false, scaleStepWidth: 1.0, scaleStartValue: 0.0, scaleShowLabels: true });
        });

        $.getJSON("../food.json", function(data) {
          $.each(data.food, function(item) {
            var start = parseISO8601Date(data.food[item].start);
            var end = parseISO8601Date(data.food[item].end);
            var difference = end - start;

            mealTimes.push(difference / 3600000.0);
          });

          var barChartData = {
            labels: [],
            datasets: [
              {
                fillColor: "#00aaff",
                strokeColor: "#00aaff",
                data: mealTimes
              }
            ]};

          $.each(mealTimes, function(item) {
            barChartData.labels.push(parseISO8601Date(data.food[item].start).toDateString());
          });

          var barChar = new Chart(document.getElementById("meal").getContext("2d")).Bar(barChartData, { animationSteps: 100, scaleOverride: true, scaleSteps: 24, scaleShowGridLines: false, scaleStepWidth: 1.0, scaleStartValue: 0.0, scaleShowLabels: true });
        });

        $.getJSON("../wanikani.json", function(data) {
          var times = [];
          $.each(data.wanikani, function(item) {
            var item = data.wanikani[item].items;

            times = item;
          });

          var barChartData = [
                  { value: times[0], color: "#ff00aa" },
                  { value: times[1], color: "#aa38c6" },
                  { value: times[2], color: "#5571e2" },
                  { value: times[3], color: "#00aaff" },
                  { value: times[4], color: "#555" }
                ];

          var pieChart = new Chart(document.getElementById("wanikani").getContext("2d")).Doughnut(barChartData);
        });

        $.getJSON("../wanikani.json", function(data) {
          var sleepAvg = sleepTimes.reduce(function(a,b) { return a + b}) / sleepTimes.length;
          var workAvg = workTimes.reduce(function(a,b) { return a + b}) / workTimes.length;
          var mealAvg = mealTimes.reduce(function(a,b) { return a + b}) / mealTimes.length;

          var radarChartData = {
            labels: ["sleep", "work", "meal"],
            datasets: [
              {
                fillColor: "#00aaff",
                strokeColor: "#00aaff",
                pointColor : "rgba(151,187,205,1)",
      pointStrokeColor : "#fff",
                data: [sleepAvg, workAvg, mealAvg]
              }
            ]};
          var radarChart = new Chart(document.getElementById("day").getContext("2d")).Radar(radarChartData);
        });
      })();